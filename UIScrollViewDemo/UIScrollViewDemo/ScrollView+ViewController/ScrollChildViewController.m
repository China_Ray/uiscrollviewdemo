//
//  ScrollChildViewController.m
//  test
//
//  Created by Admin on 15/10/13.
//  Copyright © 2015年 admin. All rights reserved.
//

#import "ScrollChildViewController.h"
#import "AViewController.h"
#import "BViewController.h"
#import "CViewController.h"
#import "UIView+frameAdjust.h"

@interface ScrollChildViewController ()

@property (strong, nonatomic) UIButton *button_back;

@property (strong, nonatomic) AViewController *aVC;
@property (strong, nonatomic) BViewController *bVC;
@property (strong, nonatomic) CViewController *cVC;

@property (strong, nonatomic) UIViewController *currentVC;

@property (strong, nonatomic) UIButton *button_A;
@property (strong, nonatomic) UIButton *button_B;
@property (strong, nonatomic) UIButton *button_C;

@property (strong, nonatomic) UIScrollView *scrollView;

@end

@implementation ScrollChildViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self layoutSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)layoutSubviews {
    
    NSLog(@"current is %@",[UIScreen mainScreen].currentMode);
    NSLog(@"current size is %@",NSStringFromCGSize([[UIScreen mainScreen] currentMode].size));
    NSLog(@"view size is %@",NSStringFromCGSize([self.view bounds].size));
    
    //返回按钮
    [self.view addSubview:self.button_back];
    
    [self.view addSubview:self.button_A];
    [self.view addSubview:self.button_B];
    [self.view addSubview:self.button_C];
    
    [self.view addSubview:self.scrollView];

    self.aVC = [[AViewController alloc] init];
    [self.aVC.view setFrame:CGRectMake(0, 0, CGRectGetWidth(self.scrollView.bounds), CGRectGetHeight(self.scrollView.bounds))];
    [self.scrollView addSubview:self.aVC.view];
    
    self.bVC = [[BViewController alloc] init];
    [self.bVC.view setFrame:CGRectMake(0, 0, CGRectGetWidth(self.scrollView.bounds), CGRectGetHeight(self.scrollView.bounds))];
    [self.bVC.view setX:CGRectGetWidth(self.scrollView.bounds)];
    [self.scrollView addSubview:self.bVC.view];
    
    self.cVC = [[CViewController alloc] init];
    [self.cVC .view setFrame:CGRectMake(0, 0, CGRectGetWidth(self.scrollView.bounds), CGRectGetHeight(self.scrollView.bounds))];
    [self.cVC.view setX:CGRectGetWidth(self.scrollView.bounds)*2];
    [self.scrollView addSubview:self.cVC.view];
    
}

#pragma mark - Response Event

- (void)buttonClick:(UIButton *)button {
    if ((self.currentVC == self.aVC && button.tag == 0)
        || (self.currentVC == self.bVC && button.tag == 1)
        || (self.currentVC == self.cVC && button.tag == 2)) {
        return ;
    }
    
    switch (button.tag) {
        case 1:
            [self moveOldViewController:self.currentVC ToViewController:self.bVC];
            break;
        case 2:
            [self moveOldViewController:self.currentVC ToViewController:self.cVC];
            break;
        case 0:
        default:
            [self moveOldViewController:self.currentVC ToViewController:self.aVC];
            break;
    }
}

- (void)moveOldViewController:(UIViewController *)old ToViewController:(UIViewController *)controller {

    [self addChildViewController:controller];
    
    [self transitionFromViewController:old
                      toViewController:controller
                              duration:0.35
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:nil
                            completion:^(BOOL finished) {
                                if (finished) {
//                                    [controller didMoveToParentViewController:self];
//                                    [old willMoveToParentViewController:nil];
//                                    [old removeFromParentViewController];
                                    NSLog(@"%@",self.childViewControllers);
                                    self.currentVC = controller;
                                } else {
                                    self.currentVC = old;
                                }
                            }];
    
}

- (void)backButtonClick:(UIButton*)button {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - lazyload

- (UIButton *)button_back {
    if (_button_back == nil) {
        _button_back = [[UIButton alloc] initWithFrame:CGRectMake(0, 20.0f, 80, 44)];
        [_button_back setTitle:@"back" forState:UIControlStateNormal];
        [_button_back setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [_button_back addTarget:self action:@selector(backButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _button_back;
}

- (UIButton *)button_A {
    if (_button_A == nil) {
        _button_A = [[UIButton alloc] initWithFrame:CGRectMake(0, 64, 100, 44)];
        [_button_A setTag:0];
        [_button_A setTitle:@"A" forState:UIControlStateNormal];
        [_button_A setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_button_A addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _button_A;
}

- (UIButton *)button_B {
    if (_button_B == nil) {
        _button_B = [[UIButton alloc] initWithFrame:CGRectMake(110, 64, 100, 44)];
        [_button_B setTag:1];
        [_button_B setTitle:@"B" forState:UIControlStateNormal];
        [_button_B setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_button_B addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _button_B;
}

- (UIButton *)button_C {
    if (_button_C == nil) {
        _button_C = [[UIButton alloc] initWithFrame:CGRectMake(220, 64, 100, 44)];
        [_button_C setTag:2];
        [_button_C setTitle:@"C" forState:UIControlStateNormal];
        [_button_C setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_button_C addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _button_C;
}

- (UIScrollView *)scrollView {
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 120, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds)-120.0f)];
        _scrollView.backgroundColor = [UIColor redColor];
        _scrollView.scrollEnabled = YES;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsHorizontalScrollIndicator = YES;
        _scrollView.contentSize = CGSizeMake(CGRectGetWidth(self.view.bounds)*3, 0);
    }
    return _scrollView;
}

@end
