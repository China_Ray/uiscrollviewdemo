//
//  PageScrollView.m
//  UIScrollViewDemo
//
//  Created by Admin on 15/10/21.
//  Copyright © 2015年 admin. All rights reserved.
//

#import "PageScrollView.h"
#import "UIView+frameAdjust.h"
#import "UIImage+Expand.h"

#define ScrollViewWidth CGRectGetWidth(self.bounds)

@interface PageScrollView () <UIScrollViewDelegate>

@property (strong, nonatomic) NSMutableArray *imageData;
@property (assign) NSInteger imageCount;

@property (assign) NSInteger leftImageIndex;
@property (assign) NSInteger currentImageIndex;
@property (assign) NSInteger rightImageIndex;
@property (assign) NSInteger currentPage;

@property (strong, nonatomic) UIImageView *leftImageView;
@property (strong, nonatomic) UIImageView *centerImageView;
@property (strong, nonatomic) UIImageView *rightImageView;

@end

@implementation PageScrollView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self loadSubviews];
    }
    return self;
}

#pragma mark - main

- (void)loadSubviews {
    self.pagingEnabled = YES;
    self.showsHorizontalScrollIndicator = NO;
    self.delegate = self;
    
    [self addSubview:self.leftImageView];
    [self addSubview:self.centerImageView];
    [self addSubview:self.rightImageView];
}

//设置显示数据
- (void)setShowData:(NSMutableArray *)_dataList {
    self.imageData = _dataList;
    self.imageCount = _dataList.count;
    
    self.currentImageIndex = 0;
    self.currentPage = 0;
    
    if (_imageCount > 1) {
        self.contentSize = CGSizeMake(CGRectGetWidth(self.bounds)*3, 0);
        self.contentOffset = CGPointMake(ScrollViewWidth, self.contentOffset.y);
        
        self.leftImageIndex = (_currentImageIndex+_imageCount-1)%_imageCount;
        self.rightImageIndex = (_currentImageIndex+1)%_imageCount;
    } else {
        self.leftImageIndex = 0;
        self.rightImageIndex = 0;
    }

    _leftImageView.image = [UIImage imageWithContentOfFile:[_imageData objectAtIndex:_leftImageIndex]];
    _centerImageView.image = [UIImage imageWithContentOfFile:[_imageData objectAtIndex:_currentImageIndex]];
    _rightImageView.image = [UIImage imageWithContentOfFile:[_imageData objectAtIndex:_rightImageIndex]];
}

#pragma mark - UIScrollViewDelegate

//拖拽滑行结束
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    //重新加载图片
    [self reloadImage];

    [scrollView setContentOffset:CGPointMake(ScrollViewWidth, 0) animated:NO];
    
    if (self.pageBlock) {
        self.pageBlock(_currentImageIndex);
    }
}

//重新加载图片
- (void)reloadImage {
    CGFloat offsetX = self.contentOffset.x;
    if (offsetX > ScrollViewWidth) {
        _currentImageIndex = (_currentImageIndex+1)%_imageCount;
    } else if (offsetX < ScrollViewWidth) {
        _currentImageIndex = (_currentImageIndex+_imageCount-1)%_imageCount;
    }
    
    _leftImageIndex = (_currentImageIndex+_imageCount-1)%_imageCount;
    _rightImageIndex = (_currentImageIndex+1)%_imageCount;
    
    _leftImageView.image = [UIImage imageWithContentOfFile:[_imageData objectAtIndex:_leftImageIndex]];
    _centerImageView.image = [UIImage imageWithContentOfFile:[_imageData objectAtIndex:_currentImageIndex]];
    _rightImageView.image = [UIImage imageWithContentOfFile:[_imageData objectAtIndex:_rightImageIndex]];
}

#pragma mark - lazyload

- (UIImageView *)leftImageView {
    if (_leftImageView == nil) {
        _leftImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ScrollViewWidth, CGRectGetHeight(self.bounds))];
        _leftImageView.backgroundColor = [UIColor clearColor];
        _leftImageView.clipsToBounds = YES;
        _leftImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _leftImageView;
}

- (UIImageView *)centerImageView {
    if (_centerImageView == nil) {
        _centerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(ScrollViewWidth, 0, ScrollViewWidth, CGRectGetHeight(self.bounds))];
        _centerImageView.backgroundColor = [UIColor clearColor];
        _centerImageView.clipsToBounds = YES;
        _centerImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _centerImageView;
}

- (UIImageView *)rightImageView {
    if (_rightImageView == nil) {
        _rightImageView = [[UIImageView alloc] initWithFrame:CGRectMake(ScrollViewWidth*2, 0, ScrollViewWidth, CGRectGetHeight(self.bounds))];
        _rightImageView.backgroundColor = [UIColor clearColor];
        _rightImageView.clipsToBounds = YES;
        _rightImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _rightImageView;
}

@end
