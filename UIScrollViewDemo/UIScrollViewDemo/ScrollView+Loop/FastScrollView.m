//
//  FastScrollView.m
//  UIScrollViewDemo
//
//  Created by Admin on 15/10/21.
//  Copyright © 2015年 admin. All rights reserved.
//

#import "FastScrollView.h"
#import "UIView+frameAdjust.h"
#import "UIImage+Expand.h"

#define ScrollViewWidth CGRectGetWidth(self.bounds)

@interface FastScrollView () <UIScrollViewDelegate>
//可见试图
@property (strong, nonatomic) NSMutableArray *visiableViews;
//复用视图
@property (strong, nonatomic) NSMutableArray *reusedViews;
//图片数据
@property (strong, nonatomic) NSMutableArray *imageData;
//图片总数
@property (assign) NSInteger imageCount;

//当前图片索引
@property (assign) NSInteger currentImageIndex;
//当前索引
@property (assign) NSInteger currentPage;

@end

@implementation FastScrollView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self loadSubviews];
    }
    return self;
}

#pragma mark - main

- (void)loadSubviews {
    self.pagingEnabled = NO;
    self.showsHorizontalScrollIndicator = NO;
    self.delegate = self;
    
    self.visiableViews = [NSMutableArray arrayWithCapacity:2];
    self.reusedViews = [NSMutableArray arrayWithCapacity:2];
    self.imageData = nil;
    self.imageCount = 0;
    self.currentImageIndex = 0;
    self.currentPage = 0;
}

//设置显示数据
- (void)setShowData:(NSMutableArray *)_dataList {
    _imageData = _dataList;
    _imageCount = _imageData.count;
    self.contentSize = CGSizeMake(CGRectGetWidth(self.bounds)*_imageCount, 0);
}

#pragma mark - UIView Layout

- (void)recenterIfNecessary {
    
    CGPoint currentOffset = [self contentOffset];
    CGFloat contentWidth = [self contentSize].width;
    CGFloat centerOffsetX = (contentWidth - [self bounds].size.width) / 2.0;
    CGFloat distanceFromCenter = fabs(currentOffset.x - centerOffsetX);
    
    if (distanceFromCenter > (contentWidth / 4.0)) {
        self.contentOffset = CGPointMake(centerOffsetX, currentOffset.y);
        for (UIView *view in self.visiableViews) {
            CGPoint center = view.center;
            center.x += (centerOffsetX - currentOffset.x);
            view.center = center;
        }
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    [self recenterIfNecessary];
    [self reloadSubviews];
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {

}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
}

//获取重用试图
- (UIImageView *)reusedImageView {
    UIImageView *imageView = [self.reusedViews firstObject];
    if (imageView) {
        [self.reusedViews removeObject:imageView];
    } else {
        imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        imageView.backgroundColor = [UIColor clearColor];
        imageView.clipsToBounds = YES;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.image = [UIImage imageWithContentOfFile:@"pic_6.jpg"];
    }
    [self addSubview:imageView];
    return imageView;
}

//更换右试图
- (CGFloat)replaceNewViewOnRight:(CGFloat)rightEdge {
    NSLog(@"replaceNewViewOnRight is %f",rightEdge);
    UIImageView *imageView = [self reusedImageView];
    [self.visiableViews addObject:imageView];

    CGRect frame = imageView.frame;
    frame.origin.x = rightEdge;
    imageView.frame = frame;
    return CGRectGetMaxX(imageView.frame);
}

//更换左试图
- (CGFloat)replaceNewViewOnLeft:(CGFloat)leftEdge {
    NSLog(@"replaceNewViewOnLeft is %f",leftEdge);
    UIImageView *imageView = [self reusedImageView];
    [self.visiableViews insertObject:imageView atIndex:0];

    CGRect frame = imageView.frame;
    frame.origin.x = leftEdge-CGRectGetWidth(self.bounds);
    imageView.frame = frame;
    return CGRectGetMinX(imageView.frame);
}

//重新加载子试图
- (void)reloadSubviews {
    CGFloat minX = CGRectGetMinX(self.bounds);
    CGFloat maxX = CGRectGetMaxX(self.bounds);
    if ([self.visiableViews count] == 0) {
        [self replaceNewViewOnRight:minX];
    }
    
    UIImageView *lastImageView = [self.visiableViews lastObject];
    CGFloat rightEdge = CGRectGetMaxX(lastImageView.frame);
    while (rightEdge < maxX) {
        rightEdge = [self replaceNewViewOnRight:rightEdge];
    }
    
    UIImageView *firstImageView = [self.visiableViews firstObject];
    CGFloat leftEdge = CGRectGetMinX(firstImageView.frame);
    while (leftEdge > minX) {
        leftEdge = [self replaceNewViewOnLeft:leftEdge];
    }
    
    lastImageView = [self.visiableViews lastObject];
    while (CGRectGetMinX(lastImageView.frame) > maxX) {
        [self.reusedViews addObject:lastImageView];
        [lastImageView removeFromSuperview];
        [self.visiableViews removeLastObject];
        lastImageView = [self.visiableViews lastObject];
    }
    
    firstImageView = [self.visiableViews firstObject];
    while (CGRectGetMaxX(firstImageView.frame) < minX) {
        [self.reusedViews addObject:firstImageView];
        [firstImageView removeFromSuperview];
        [self.visiableViews removeObjectAtIndex:0];
        firstImageView = [self.visiableViews firstObject];
    }
}

@end
