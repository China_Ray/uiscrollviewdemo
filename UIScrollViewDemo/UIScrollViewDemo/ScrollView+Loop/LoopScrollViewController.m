//
//  LoopScrollViewController.m
//  test
//
//  Created by Admin on 15/10/19.
//  Copyright © 2015年 admin. All rights reserved.
//

#import "LoopScrollViewController.h"
#import "FastScrollView.h"
#import "PageScrollView.h"
#import "AppleScrollView.h"

#define ScrollViewRect CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 300.0f)

@interface LoopScrollViewController () <UIScrollViewDelegate>

//返回按钮
@property (strong, nonatomic) UIButton *button_back;
//分页滚动试图
@property (strong, nonatomic) PageScrollView *pageScrollView;
//快速滚动试图
@property (strong, nonatomic) FastScrollView *fastScrollView;

//分页试图
@property (strong, nonatomic) UIPageControl *pageControl;
//当前页码
@property (assign) NSInteger currentPage;

//图片数据
@property (strong, nonatomic) NSMutableArray *imageDataList;

@end

@implementation LoopScrollViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    [self layoutSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

#pragma mark - main

- (void)layoutSubviews {
    //加载子试图
    [self loadSubviews];
    //加载数据
    [self loadData];
}

//加载子试图
- (void)loadSubviews {
    //返回按钮
    [self.view addSubview:self.button_back];
    //滑动列表
    [self.view addSubview:self.fastScrollView];
    //分页
    [self.view addSubview:self.pageControl];
}

//加载数据
- (void)loadData {
    //图片数据列表
    _imageDataList = [[NSMutableArray alloc] initWithObjects:@"pic_1.jpg",@"pic_2.jpg",@"pic_3.jpg",@"pic_4.jpg",@"pic_5.jpg",@"pic_6.jpg",nil];
    
    CGSize size = [_pageControl sizeForNumberOfPages:_imageDataList.count];
    [_pageControl setFrame:CGRectMake((CGRectGetWidth(self.view.bounds)-size.width)/2, CGRectGetHeight(ScrollViewRect), size.width, size.height)];
    [_pageControl setNumberOfPages:_imageDataList.count];
    [_pageControl setCurrentPage:0];
    
    __weak typeof(self)weakSelf = self;
    [self.fastScrollView setShowData:_imageDataList];
    self.fastScrollView.pageBlock = ^(NSInteger currentPage){
        [weakSelf.pageControl setCurrentPage:currentPage];
    };
}

#pragma amrk - UIResponse Event

- (void)backButtonClick:(UIButton*)button {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - lazyload

- (UIButton *)button_back {
    if (_button_back == nil) {
        _button_back = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.bounds)-64.0f, 100, 44)];
        [_button_back setTitle:@"返回" forState:UIControlStateNormal];
        [_button_back setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [_button_back addTarget:self action:@selector(backButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _button_back;
}

- (FastScrollView *)fastScrollView {
    if (_fastScrollView == nil) {
        _fastScrollView = [[FastScrollView alloc] initWithFrame:ScrollViewRect];
        _fastScrollView.backgroundColor = [UIColor clearColor];
    }
    return _fastScrollView;
}

- (PageScrollView *)pageScrollView {
    if (_pageScrollView == nil) {
        _pageScrollView = [[PageScrollView alloc] initWithFrame:ScrollViewRect];
    }
    return _pageScrollView;
}

- (UIPageControl *)pageControl {
    if (_pageControl == nil) {
        _pageControl = [[UIPageControl alloc] init];
        _pageControl.pageIndicatorTintColor = [UIColor grayColor];
        _pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
    }
    return _pageControl;
}

@end
