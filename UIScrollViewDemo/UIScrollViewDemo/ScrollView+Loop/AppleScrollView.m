//
//  AppleScrollView.m
//  UIScrollViewDemo
//
//  Created by Admin on 15/10/21.
//  Copyright © 2015年 admin. All rights reserved.
//

#import "AppleScrollView.h"
#import "UIView+frameAdjust.h"
#import "UIImage+Expand.h"

@interface AppleScrollView () <UIScrollViewDelegate>

//可见试图
@property (strong, nonatomic) NSMutableArray *visiableViews;
//图片数据
@property (strong, nonatomic) NSMutableArray *imageData;
//图片总数
@property (assign) NSInteger imageCount;
//当前索引
@property (assign) NSInteger imageIndex;

@end

@implementation AppleScrollView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator = NO;
        self.delegate = self;
        
        self.visiableViews = [NSMutableArray arrayWithCapacity:2];
        self.imageData = nil;
        self.imageCount = 0;
        self.imageIndex = 0;
    }
    return self;
}

//设置显示数据
- (void)setScrollData:(NSMutableArray *)_data {
    self.imageData = _data;
    self.imageCount = _data.count;
    self.contentSize = CGSizeMake(CGRectGetWidth(self.bounds)*_imageCount, 0);
}

#pragma mark - layout

- (void)recenterIfNecessary {
    CGPoint currentOffset = [self contentOffset];
    CGFloat contentWidth = [self contentSize].width;
    CGFloat centerOffsetX = (contentWidth - [self bounds].size.width) / 2.0;
    CGFloat distanceFromCenter = fabs(currentOffset.x - centerOffsetX);
    
    if (distanceFromCenter > (contentWidth / 4.0)) {
        self.contentOffset = CGPointMake(centerOffsetX, currentOffset.y);
        
        for (UIView *view in self.visiableViews) {
            CGPoint center = view.center;
            center.x += (centerOffsetX - currentOffset.x);
            view.center = center;
        }
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (_imageData == nil || _imageCount == 0) {
        return ;
    }
    
    [self recenterIfNecessary];
    
    CGRect visiableRect = self.bounds;
    CGFloat minX = CGRectGetMinX(visiableRect);
    CGFloat maxX = CGRectGetMaxX(visiableRect);
    [self reloadSubviewsFromMinX:minX toMaxX:maxX];
}

#pragma mark - main

//插入新试图
- (UIImageView *)insertImageView {
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    [imageView setBackgroundColor:[UIColor clearColor]];
    [imageView setClipsToBounds:YES];
    [self addSubview:imageView];
    
    return imageView;
}

//更换右试图
- (CGFloat)replaceNewViewOnRight:(CGFloat)rightEdge {
    NSLog(@"replaceNewViewOnRight edge is %f",rightEdge);
    UIImageView *imageView = [self insertImageView];
    if ([self.visiableViews count] > 0) {
        _imageIndex = (_imageIndex+1)%_imageCount;
    }
    imageView.image = [UIImage imageWithContentOfFile:[_imageData objectAtIndex:_imageIndex]];
    [self.visiableViews addObject:imageView];
    
    [imageView setX:rightEdge];
    return CGRectGetMaxX(imageView.frame);
}

//更换左试图
- (CGFloat)replaceNewViewOnLeft:(CGFloat)leftEdge {
    NSLog(@"replaceNewViewOnLeft edge is %f",leftEdge);
    UIImageView *imageView = [self insertImageView];
    _imageIndex = (_imageIndex+_imageCount-1)%_imageCount;
    imageView.image = [UIImage imageWithContentOfFile:[_imageData objectAtIndex:_imageIndex]];
    [self.visiableViews insertObject:imageView atIndex:0];
    
    [imageView setX:leftEdge-CGRectGetWidth(self.bounds)];
    return CGRectGetMinX(imageView.frame);
}

//重新加载子试图
- (void)reloadSubviewsFromMinX:(CGFloat)minX toMaxX:(CGFloat)maxX {
    //    NSLog(@"min x is %f, max x is %f",minX,maxX);
    if ([self.visiableViews count] == 0) {
        [self replaceNewViewOnRight:minX];
    }
    
    UIImageView *lastImageView = [self.visiableViews lastObject];
    CGFloat rightEdge = CGRectGetMaxX(lastImageView.frame);
    while (rightEdge < maxX) {
        rightEdge = [self replaceNewViewOnRight:rightEdge];
    }
    
    UIImageView *firstImageView = [self.visiableViews firstObject];
    CGFloat leftEdge = CGRectGetMinX(firstImageView.frame);
    while (leftEdge > minX) {
        leftEdge = [self replaceNewViewOnLeft:leftEdge];
    }
    
    lastImageView = [self.visiableViews lastObject];
    while (CGRectGetMinX(lastImageView.frame) > maxX) {
        [lastImageView removeFromSuperview];
        [self.visiableViews removeLastObject];
        lastImageView = [self.visiableViews lastObject];
    }
    
    firstImageView = [self.visiableViews firstObject];
    while (CGRectGetMaxX(firstImageView.frame) < minX) {
        [firstImageView removeFromSuperview];
        [self.visiableViews removeObjectAtIndex:0];
        firstImageView = [self.visiableViews firstObject];
    }
}

@end
