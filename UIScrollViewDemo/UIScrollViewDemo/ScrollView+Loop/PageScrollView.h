//
//  PageScrollView.h
//  UIScrollViewDemo
//  功能介绍 - 分页循环滚动
//  Created by Admin on 15/10/21.
//  Copyright © 2015年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^PageControlBlock)(NSInteger currentPage);

@interface PageScrollView : UIScrollView

@property (copy, nonatomic) PageControlBlock pageBlock;

//设置显示数据
- (void)setShowData:(NSMutableArray *)_dataList;

@end
