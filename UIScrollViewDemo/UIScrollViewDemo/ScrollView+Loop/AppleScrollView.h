//
//  AppleScrollView.h
//  UIScrollViewDemo
//  功能介绍 - 苹果示例代码
//  Created by Admin on 15/10/21.
//  Copyright © 2015年 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppleScrollView : UIScrollView

//设置显示数据
- (void)setScrollData:(NSMutableArray *)_data;

@end
