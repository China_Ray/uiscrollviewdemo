//
//  MainViewController.m
//  test
//
//  Created by Admin on 15/10/19.
//  Copyright © 2015年 admin. All rights reserved.
//

#import "MainViewController.h"
#import "ScrollChildViewController.h"
#import "LoopScrollViewController.h"

@interface MainViewController ()

@property (strong, nonatomic) UIButton *button_A;
@property (strong, nonatomic) UIButton *button_B;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self layoutSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - main

- (void)layoutSubviews {
    [self.view addSubview:self.button_A];
    [self.view addSubview:self.button_B];
}

#pragma mark - UIResponse Event

- (void)buttonClick:(UIButton *)button {
    if (button.tag == 0) {
        [self gotoScrollChild];
    } else if (button.tag == 1) {
        [self gotoScrollLoop];
    }
}

//UIScrollView + UIViewController
- (void)gotoScrollChild {
    ScrollChildViewController *viewController = [[ScrollChildViewController alloc] init];
    [self presentViewController:viewController animated:YES completion:nil];
}

//UIScrollView + Loop
- (void)gotoScrollLoop {
    LoopScrollViewController *viewController = [[LoopScrollViewController alloc] init];
    [self presentViewController:viewController animated:YES completion:nil];
}

#pragma mark - lazyload

- (UIButton *)button_A {
    if (_button_A == nil) {
        _button_A = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 44)];
        [_button_A setTag:0];
        [_button_A setCenter:CGPointMake(CGRectGetWidth(self.view.bounds)/2, CGRectGetHeight(self.view.bounds)/2-25)];
        [_button_A setTitle:@"多个ViewController子试图" forState:UIControlStateNormal];
        [_button_A setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_button_A addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _button_A;
}

- (UIButton *)button_B {
    if (_button_B == nil) {
        _button_B = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 44)];
        [_button_B setTag:1];
        [_button_B setCenter:CGPointMake(CGRectGetWidth(self.view.bounds)/2, CGRectGetHeight(self.view.bounds)/2+25)];
        [_button_B setTitle:@"循环滚动" forState:UIControlStateNormal];
        [_button_B.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [_button_B.titleLabel setNumberOfLines:0];
        [_button_B setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_button_B addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _button_B;
}

@end
