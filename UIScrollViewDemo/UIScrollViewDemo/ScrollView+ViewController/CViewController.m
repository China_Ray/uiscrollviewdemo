//
//  CViewController.m
//  test
//
//  Created by Admin on 15/10/13.
//  Copyright © 2015年 admin. All rights reserved.
//

#import "CViewController.h"

@interface CViewController ()

@property (strong, nonatomic) UILabel *wordLabel;

@end

@implementation CViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"C view did load");
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSLog(@"C view will appear");
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSLog(@"C view did appear");
    [self layoutSubviews];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    NSLog(@"C view will disappear");
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    NSLog(@"C view did disappear");
}

- (void)layoutSubviews {
    self.view.backgroundColor = [UIColor blueColor];
    [self.view addSubview:self.wordLabel];
}

- (UILabel *)wordLabel {
    if (_wordLabel == nil) {
        _wordLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 60.0f)];
        _wordLabel.backgroundColor = [UIColor whiteColor];
        _wordLabel.center = self.view.center;
        _wordLabel.textAlignment = NSTextAlignmentCenter;
        _wordLabel.textColor = [UIColor blackColor];
        _wordLabel.font = [UIFont systemFontOfSize:20.0f];
        _wordLabel.text = @"C";
    }
    return _wordLabel;
}

@end
