//
//  AViewController.m
//  test
//
//  Created by Admin on 15/10/13.
//  Copyright © 2015年 admin. All rights reserved.
//

#import "AViewController.h"

@interface AViewController ()

@property (strong, nonatomic) UILabel *wordLabel;

@end

@implementation AViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"A view did load");
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSLog(@"A view will appear");
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSLog(@"A view did appear");
    [self layoutSubviews];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    NSLog(@"A view will disappear");
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    NSLog(@"A view did disappear");
}

- (void)layoutSubviews {
    self.view.backgroundColor = [UIColor grayColor];
    [self.view addSubview:self.wordLabel];
}

- (UILabel *)wordLabel {
    if (_wordLabel == nil) {
        _wordLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 60.0f)];
        _wordLabel.backgroundColor = [UIColor whiteColor];
        _wordLabel.center = self.view.center;
        _wordLabel.textAlignment = NSTextAlignmentCenter;
        _wordLabel.textColor = [UIColor blackColor];
        _wordLabel.font = [UIFont systemFontOfSize:20.0f];
        _wordLabel.text = @"A";
    }
    return _wordLabel;
}

@end
